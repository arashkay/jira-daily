import React, { useState } from "react";
import moment from "moment";
import { Badge } from "antd";
import {
  filter,
  some,
  orderBy,
  find,
  differenceBy,
  isArray,
  sumBy,
  isEmpty,
} from "lodash";
import Row from "../row/Row";
import "./Roadmap.css";

const countIssuesInADay = (issues, date) =>
  issues.filter(
    ({ dates }) => !!find(dates, (item) => item.isSame(date, "day"))
  ).length;

const issuesIsInADay = (dates, date) =>
  !!find(dates, (item) => item.isSame(date, "day"));

const Roadmap = ({
  issues,
  sprint,
  scrollable,
  startDate,
  endDate,
  selectedAvatars,
  parentOn,
  onlyWrongIssues,
  showMainFeatures,
  showSecondaryFeatures,
  onlyToday,
}) => {
  const completedCount = filter(
    issues,
    ({ status: { name } }) => name === "Done"
  ).length;
  const inProgressCount = filter(
    issues,
    ({ status: { name } }) => name === "In Progress"
  ).length;
  const notStartedCount = filter(
    issues,
    ({ status: { name } }) => name === "Open"
  ).length;
  const totalEstimated = Math.round(
    sumBy(issues, ({ timeoriginalestimate }) => timeoriginalestimate) / 3600
  );

  const totalSpent = Math.round(
    sumBy(issues, ({ status: { name }, timespent, timeoriginalestimate }) =>
      timespent > timeoriginalestimate || name === "Done"
        ? timeoriginalestimate
        : timespent
    ) / 3600
  );

  const mainIssues = filter(issues, ({ isSecondary }) => !isSecondary);
  const otherIssues = filter(issues, ({ isSecondary }) => isSecondary);
  const duration = moment(endDate).diff(startDate, "days") + 1;
  const cols = [];
  const weekends = [];
  let currentDistanceInSprint = 0;
  for (let i = 0; i < duration; i++) {
    const date = moment(startDate).add(i, "day");
    let isWeekend = [5, 6].includes(date.day());
    if (isWeekend) {
      weekends.push(i);
    }
    if (moment().isAfter(date) && !isWeekend) {
      currentDistanceInSprint++;
    }
    // create a day
    cols.push(
      <div
        className={`w-day text-center ${
          isWeekend ? "bg-gray-300" : i % 2 === 0 ? "w-day-alt" : ""
        }`}
      >
        {date.format("ddd")}
        <div className="text-xs flex items-center flex-col text-gray-500">
          <span>{date.format("Do")}</span>
          <Badge
            className="py-1"
            style={{ backgroundColor: "#1a202c" }}
            count={countIssuesInADay(issues, date)}
          />
        </div>
      </div>
    );
  }
  const weekDaysCount = duration - weekends.length;
  const totalProgress = Math.round((100 * totalSpent) / totalEstimated);
  const idealProgress = Math.round(
    (100 * currentDistanceInSprint) / weekDaysCount
  );

  const renderIssue = (issue, type) => {
    if (onlyToday && !issuesIsInADay(issue.dates, moment())) {
      return null;
    }
    if (
      (parentOn && issue.isParent) ||
      isEmpty(selectedAvatars) ||
      (!isEmpty(selectedAvatars) &&
        issue.assignee &&
        some(
          selectedAvatars,
          ({ accountId }) => accountId === issue.assignee.accountId
        ))
    )
      return (
        <Row
          issue={issue}
          sprintStartDate={startDate}
          sprintEndDate={endDate}
          duration={duration}
          weekends={weekends}
          onlyWrongIssues={onlyWrongIssues}
        />
      );
  };

  return (
    <div className="roadmap text-left mx-auto mt-2">
      <div className="roadmap-header flex absolute flex-row bg-white z-10 shadow-sm">
        <div className="w-task w-task-title">
          <div>Tasks</div>
          <div className="flex">
            <Badge
              className="mr-1"
              title="Done"
              style={{ backgroundColor: "#48BB78" }}
              count={completedCount}
              overflowCount={999}
            />
            <Badge
              className="mr-1"
              title="In Progress"
              style={{ backgroundColor: "#4299E1" }}
              count={inProgressCount}
              overflowCount={999}
            />
            <Badge
              className="flex"
              title="Open"
              style={{ backgroundColor: "#1a202c" }}
              count={notStartedCount}
              overflowCount={999}
            />
          </div>
          <div className="flex items-center mt-1">
            <span>Expected {idealProgress}%</span>
            <Badge
              className="ml-1"
              title="Actual Progress"
              style={{ backgroundColor: "#1a202c" }}
              count={`${totalProgress}%`}
            />
          </div>
        </div>
        <div className="w-duration text-center">Days</div>
        <div className="flex flex-grow">{cols}</div>
      </div>
      <div
        className={`relative roadmap-body overflow-y-scroll ${
          [
            "roadmap-body-scroll",
            "roadmap-body-900",
            "roadmap-body-600",
            "roadmap-body-300",
          ][scrollable]
        }`}
      >
        {showMainFeatures && mainIssues.map(renderIssue)}
        {showSecondaryFeatures &&
          otherIssues.map((issue) => renderIssue(issue, "secondary"))}
      </div>
    </div>
  );
};

export default Roadmap;
