import React, { useState, useEffect } from "react";
import { Avatar, Badge } from "antd";
import { CheckOutlined, VideoCameraOutlined } from "@ant-design/icons";
import moment from "moment";
import { filter, find } from "lodash";
import "./Row.css";

const Row = ({
  issue: {
    key,
    isParent,
    summary,
    issuetype,
    startDate,
    endDate,
    correctEndDate,
    parent,
    timeoriginalestimate,
    progressPercentage,
    progress,
    assignee,
    status: {
      name: statusName,
      statusCategory: { key: status },
    },
    demoLink,
  },
  sprintStartDate,
  sprintEndDate,
  duration,
  weekends,
  onlyWrongIssues,
  onlyToday,
}) => {
  const { avatarUrls } = assignee || {};
  const colors = {
    Release: "bg-gray-500",
    Test: "bg-gray-500",
    Review: "bg-gray-500",
    Chore: "bg-blue-300",
    Design: "bg-blue-300",
    Feat: "bg-blue-300",
    Story: "bg-blue-400",
    Fix: "bg-yellow-400",
  };

  const startPoint = startDate.diff(sprintStartDate, "days");
  const size = endDate.diff(startDate, "days") + 1;
  const wrongDueDate =
    startPoint < 0 || moment(endDate).isAfter(sprintEndDate) || correctEndDate;

  const durationInHours = timeoriginalestimate / 3600;
  const durationInMinutes = timeoriginalestimate / 60;

  const title = parent
    ? summary.indexOf("Demo") === 0 ||
      summary.indexOf("Design") === 0 ||
      summary.indexOf("API") === 0 ||
      summary.indexOf("Web") === 0 ||
      summary.indexOf("Mobile") === 0
      ? summary.split(" ")[0]
      : issuetype.name
    : summary;

  const cols = [];
  for (let i = 0; i < duration; i++) {
    const isWeekend = weekends.includes(i);
    cols.push(
      <div
        className={`w-day-map text-center ${
          isWeekend ? "bg-gray-300" : i % 2 === 0 ? "w-day-alt" : ""
        }`}
      />
    );
  }

  if (onlyWrongIssues && !wrongDueDate) {
    return null;
  }

  return (
    <div class="row flex flex-row items-stretch">
      <div
        title={summary}
        className={`w-task ${parent ? "pl-3 pr-1" : "px-1 font-bold"} ${
          wrongDueDate
            ? "text-red-600 line-through"
            : parent
            ? "text-gray-700"
            : ""
        }`}
      >
        {status === "done" ? (
          <Avatar
            icon={<CheckOutlined style={{ fontSize: "10px" }} />}
            size={16}
            className="mr-1 bg-green-600"
            style={{ lineHeight: "15px" }}
          />
        ) : (
          assignee && (
            <Avatar
              src={status === "done" ? null : avatarUrls["16x16"]}
              size={16}
              className="mr-1"
            />
          )
        )}
        {demoLink && (
          <a href={demoLink} rel="noopener noreferrer" target="_blank">
            <VideoCameraOutlined className="mr-1 align-middle text-blue-500" />
          </a>
        )}
        <span
          className={
            isParent
              ? ""
              : {
                  "In Review": "text-yellow-600",
                  "In Progress": "text-blue-600",
                }[statusName]
          }
        >
          <a
            href={`https://pixelsandcode.atlassian.net/browse/${key}`}
            rel="noopener noreferrer"
            target="_blank"
          >
            {key}
          </a>{" "}
          {title}
        </span>
      </div>
      <div className="w-duration text-center">
        <Badge
          dot
          count={progressPercentage > 100 ? `${progressPercentage}%` : 0}
        >
          {size}D
        </Badge>
      </div>
      <div
        className="flex items-stretch flex-grow relative"
        style={{ width: `${50 * duration}px` }}
      >
        <div className="row-grid items-stretch flex flex-grow">{cols}</div>
        <div className="row-grid items-stretch absolute inset-x-0 flex flex-grow">
          <div
            title={`${progress.percent || 0}% - ${summary}`}
            className={`flex absolute self-end pr-2 flex-row items-center justify-left overflow-hidden whitespace-no-wrap ${
              wrongDueDate
                ? "bg-red-600 line-through"
                : colors[issuetype.name] || "bg-black"
            } bar bar-${size} offset-${startPoint}`}
          >
            <div
              className={`fill ${status === "done" ? "done" : ""}`}
              style={{
                width: `${status === "done" ? 100 : progress.percent}%`,
              }}
            />
            <div className="bar-content flex items-center">
              {assignee && !demoLink && (
                <Avatar
                  src={avatarUrls["16x16"]}
                  size={16}
                  className="mx-1 flex-shrink-0"
                />
              )}
              {demoLink && (
                <a
                  className="demo-file"
                  href={demoLink}
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <Avatar
                    icon={
                      <VideoCameraOutlined className="mr-1 align-middle text-white demo-file-icon" />
                    }
                    size={16}
                    className="mx-1 flex-shrink-0 bg-black"
                  />
                </a>
              )}
              <span className="pr-1">
                {!parent && issuetype.name === "Story" ? (
                  <span>{moment(endDate).diff(startDate, "days") + 1}D</span>
                ) : durationInHours < 1 ? (
                  <span>{Math.round(durationInMinutes)}M</span>
                ) : (
                  <span className={durationInHours > 7 ? "text-red-500" : ""}>
                    {durationInHours}H
                  </span>
                )}
              </span>
              {summary}
              {wrongDueDate &&
                ` (${startDate.format("MMM DD")} - ${endDate.format(
                  "MMM DD"
                )})`}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Row;
