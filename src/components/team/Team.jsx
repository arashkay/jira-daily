import React, { useState, useEffect } from "react";
import { Avatar, Popover, Badge } from "antd";
import { find, orderBy, some } from "lodash";

const Team = ({ team, selectedAvatars, onAvatarChange }) => {
  const avatarStyle = (member) => {
    const isSelected = some(
      selectedAvatars,
      ({ accountId }) => accountId === member.accountId
    );
    let className = isSelected ? "-mt-2 " : "";
    if (member.errors) className += "border-red-500 ";
    else {
      className += isSelected ? "border-gray-800" : "border-transparent";
    }
    return className;
  };

  return (
    <div className="space-x-1 mt-3">
      {orderBy(team, ["displayName"], ["asc"]).map((member) => {
        const hours = Math.round(member.totalEstimate / 3600);
        return (
          <Popover
            placement="bottom"
            content={
              <div>
                <div>
                  Estimated Work{" "}
                  <Badge
                    style={{
                      backgroundColor: hours < 65 ? "#4299E1" : "#F56565",
                    }}
                    count={`${hours}H`}
                  />
                </div>
                {member.errors > 0 && (
                  <div>
                    Issues with errors{" "}
                    <Badge
                      style={{
                        backgroundColor: "#F56565",
                      }}
                      count={member.errors}
                    />
                  </div>
                )}
              </div>
            }
          >
            <Avatar
              src={member.avatarUrls["32x32"]}
              onClick={() => onAvatarChange(member)}
              className={`border-2 cursor-pointer  hover:border-blue-500 ${avatarStyle(
                member
              )}`}
            />
          </Popover>
        );
      })}
    </div>
  );
};

export default Team;
