import React from "react";
import Options from "./scenes/Options";
import Home from "./scenes/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/options">
            <Options />
          </Route>
          <Route path="/index.html">
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
