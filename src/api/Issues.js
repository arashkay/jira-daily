import axios from "../configs/gateway";
import moment from "moment";
import {
  filter,
  some,
  orderBy,
  differenceBy,
  isArray,
  find,
  isEmpty,
} from "lodash";
import issuesJson from "../data/issues.json";

const secondsToDaysInMilliseconds = (seconds) => {
  const days = Math.floor(seconds / 28800);
  const remaining = seconds % 28800;
  return (days * 86400 + remaining) * 1000;
};

const getParent = (issuelinks) => {
  const parents = filter(
    issuelinks,
    ({ type: { outward } }) => outward === "blocks"
  );
  return parents.length === 0 || !parents[0].outwardIssue
    ? null
    : parents[0].outwardIssue.key;
};

const getErrors = ({
  duedate,
  assignee,
  customfield_10057,
  customfield_10058,
  issuetype: { name: type },
}) => {
  let errors = [];
  if (!duedate) errors.push("Duedate is missing");
  if (!assignee) errors.push("No assignee");
  if (type === "Story") {
    if (isEmpty(customfield_10057)) errors.push("No demo master");
    if (isEmpty(customfield_10058)) errors.push("No team");
  }
  return isEmpty(errors) ? null : errors;
};

const sortIssues = (issues) => {
  // List stories by duedate first
  const orderedIssues = orderBy(issues, ["duedate"], ["asc"]);
  // List only parent stories first
  const parents = filter(
    orderedIssues,
    ({ summary, issuelinks, issuetype }) => {
      const hasParent = some(
        issuelinks,
        ({ type: { outward }, outwardIssue }) =>
          outward === "blocks" && outwardIssue
      );
      return !(issuelinks && hasParent) && issuetype.name === "Story";
    }
  );
  const orderByParents = [];
  // Set parent start, end dates
  parents.forEach((item) => {
    const parent = { ...item, isParent: true };
    orderByParents.push(parent);
    let totalTimeSpent = 0;
    let totalEstimate = 0;
    orderedIssues.forEach((issue) => {
      if (issue.parent === parent.key) {
        orderByParents.push(issue);
        parent.dates = isArray(parent.dates)
          ? [...parent.dates, ...issue.dates]
          : [issue.dates];
        if (!parent.start || parent.startDate.isAfter(issue.startDate)) {
          parent.startDate = issue.startDate;
        }
        if (parent.endDate.isBefore(issue.endDate)) {
          parent.correctEndDate = issue.endDate;
        }
        totalTimeSpent +=
          issue.status.statusCategory.key === "done" ||
          issue.timespent > issue.timeoriginalestimate
            ? issue.timeoriginalestimate
            : issue.timespent;
        totalEstimate += issue.timeoriginalestimate;
      }
    });
    parent.progressPercentage = Math.round(
      (100 * totalTimeSpent) / totalEstimate
    );
    parent.progress = {
      percent: parent.progressPercentage,
    };
  });
  const otherIssues = differenceBy(issues, orderByParents, "key").map(
    (issue) => ({
      ...issue,
      isSecondary: true,
    })
  );
  return [...orderByParents, ...otherIssues];
};

const getIssues = (sprint, page) => {
  return axios.get("rest/api/3/search", {
    params: {
      jql: `project = TP AND Sprint = ${sprint.id}`,
      startAt: page * 100,
      maxResults: 100,
    },
  });
};

const getAllIssues = async (sprint) => {
  const { total, issues } = await getIssues(sprint, 0);
  let allIssues = [...issues];
  const maxPage = Math.floor(total / 100);
  const batch = [];
  for (let i = 1; i <= maxPage; i++) {
    batch.push(getIssues(sprint, i));
  }
  const results = await Promise.all(batch);
  results.forEach(({ issues }) => {
    allIssues = [...allIssues, ...issues];
  });
  // console.log(JSON.stringify(allIssues));

  // const allIssues = issuesJson;
  return sortIssues(
    allIssues.map((issue) => {
      const {
        key,
        self,
        fields: {
          aggregateprogress,
          progress,
          assignee,
          created,
          creator,
          duedate,
          issuelinks,
          issuetype,
          labels,
          reporter,
          status,
          summary,
          timeestimate,
          timeoriginalestimate,
          timespent,
          customfield_10070,
        },
      } = issue;
      const end = new Date(duedate).getTime();
      const start = end - secondsToDaysInMilliseconds(timeestimate);
      const endDate = moment(moment(end).format("YYYY-MM-DD"));
      const startDate = moment(moment(start).format("YYYY-MM-DD"));
      const dates = [];
      const progressPercentage =
        status.statusCategory.key === "done"
          ? 100
          : Math.round((timespent * 100) / timeoriginalestimate);

      let date = moment(startDate);
      while (date.isSameOrBefore(endDate)) {
        dates.push(moment(date));
        date.add(1, "day");
      }
      const parent = getParent(issuelinks);
      const errors = getErrors(issue.fields);
      return {
        key,
        self,
        aggregateprogress,
        progressPercentage,
        progress,
        assignee,
        created,
        creator,
        duedate,
        issuelinks,
        issuetype,
        labels,
        reporter,
        status,
        summary,
        timeestimate,
        timeoriginalestimate,
        timespent,
        start,
        startDate,
        end,
        endDate,
        dates,
        parent,
        errors,
        demoLink: customfield_10070,
      };
    })
  );
};

export { getIssues, getAllIssues };
