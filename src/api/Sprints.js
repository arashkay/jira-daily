import axios from "../configs/gateway";
import sprintJson from "../data/sprint.json";

const getSprints = async (boardId) => {
  // return sprintJson;
  const sprints = await axios.get(`rest/agile/1.0/board/${boardId}/sprint`, {
    params: { state: "active,future" },
  });
  // console.log(JSON.stringify(sprints));
  return sprints.values;
};

export { getSprints };
