/* global chrome */

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Options = () => {
  const storage = chrome.storage.sync;
  const [options, setOptions] = useState({
    domain: "",
    username: "",
    token: "",
  });

  useEffect(() => {
    storage.get((data) => {
      setOptions(data);
    });
  }, []);

  const Save = () => {
    console.log(options);
    storage.set(options);
  };

  return (
    <div className="container mx-auto mt-5">
      <Link to="/index.html">Home</Link>
      <div id="login-form" className="space-y-3">
        <h1>Jira Account Information</h1>
        <div className="md:w-2/3">
          <input
            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"
            name="domain"
            type="text"
            placeholder="Your Jira Subdomain"
            value={options.domain}
            onChange={({ target: { value: domain } }) => {
              setOptions({ ...options, domain });
            }}
          />
          <div className="text-gray-500 py-2">
            Only subdomain part of your address (e.g. projectX in https://
            <span className="font-bold text-blue-500">projectX</span>
            .atlassian.net )
          </div>
        </div>
        <div className="md:w-2/3">
          <input
            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"
            name="username"
            type="text"
            placeholder="Username"
            value={options.username}
            onChange={({ target: { value: username } }) => {
              setOptions({ ...options, username });
            }}
          />
        </div>
        <div className="md:w-2/3">
          <input
            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"
            name="token"
            type="text"
            placeholder="Token"
            value={options.token}
            onChange={({ target: { value: token } }) => {
              setOptions({ ...options, token });
            }}
          />
          <div className="text-gray-500 py-2">
            Get your token from
            <a
              target="_blank"
              href="https://id.atlassian.com/manage/api-tokens"
            >
              https://id.atlassian.com/manage/api-tokens
            </a>
          </div>
        </div>
        <div className="md:w-2/3">
          <button
            id="login"
            onClick={Save}
            className="bg-black border border-transparent hover:border-gray-500 text-white font-bold py-2 px-4 rounded"
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
};

export default Options;
