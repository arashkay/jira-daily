/* global chrome */

import React, { useState, useEffect } from "react";
import { filter, some, orderBy, isEmpty, find, findIndex } from "lodash";
import moment from "moment";
import {
  Input,
  Layout,
  Select,
  Spin,
  Menu,
  Dropdown,
  DatePicker,
  Switch,
  Collapse,
  List,
  Tag,
  Avatar,
} from "antd";
import {
  LoadingOutlined,
  MoreOutlined,
  ReloadOutlined,
  FullscreenOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { getAllIssues } from "../api/Issues";
import { getSprints } from "../api/Sprints";
import Roadmap from "../components/roadmap/Roadmap";
import Team from "../components/team/Team";
import "./Home.css";

const { Header, Content } = Layout;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { Panel } = Collapse;

const Home = () => {
  const [selectedSprint, setSelectedSprint] = useState({});
  const [team, setTeam] = useState([]);
  const [selectedAvatars, setSelectedAvatars] = useState([]);
  const [dateRange, setDateRange] = useState([
    moment(),
    moment().add(13, "days"),
  ]);
  const [sprints, setSprints] = useState([]);
  const [issues, setIssues] = useState([]);
  const [filters, setFilters] = useState({
    boardId: 16,
  });
  const [parentOn, setParentOn] = useState(true);
  const [showErrors, setShowErrors] = useState(false);
  const [scrollable, setScrollable] = useState(2);
  const [onlyToday, setOnlyToday] = useState(false);
  const [showMainFeatures, setShowMainFeatures] = useState(true);
  const [showSecondaryFeatures, setShowSecondaryFeatures] = useState(false);

  const setCurrentSprint = (sprint) => {
    if (sprint.startDate && sprint.endDate) {
      setDateRange([
        moment(moment.utc(sprint.startDate).format("YYYY-MM-DD")),
        moment(moment.utc(sprint.endDate).format("YYYY-MM-DD")),
      ]);
    }
    setSelectedSprint(sprint);
    setIssues([]);
    getAllIssues(sprint).then((issues) => {
      setIssues(issues);
      getTeam(issues);
    });
  };

  const getTeam = (issues) => {
    const avatars = [];
    issues.forEach(({ assignee, timeoriginalestimate, errors }) => {
      if (assignee) {
        let match = find(
          avatars,
          ({ accountId }) => accountId === assignee.accountId
        );
        if (!match) {
          match = { ...assignee, totalEstimate: 0 };
          avatars.push(match);
        }
        match.totalEstimate = match.totalEstimate + timeoriginalestimate;
        if (errors) match.errors = (match.errors || 0) + 1;
      }
    });
    setTeam(avatars);
  };

  const handleSprintChange = (value) => {
    const sprint = find(sprints, ["id", value]);
    setCurrentSprint(sprint);
  };

  const handleDateChange = (dates) => {
    setDateRange(dates);
  };

  const handleAvatarChange = (avatar) => {
    const index = findIndex(
      selectedAvatars,
      ({ accountId }) => accountId === avatar.accountId
    );
    setSelectedAvatars(
      index > -1
        ? selectedAvatars.filter((item, i) => index !== i)
        : [...selectedAvatars, avatar]
    );
  };

  useEffect(() => {
    getSprints(filters.boardId).then((sprints) => {
      setSprints(sprints);
      let sprint = find(sprints, ["state", "active"]);
      if (!sprint) sprint = sprints[0];
      setCurrentSprint(sprint);
    });
  }, []);

  const menu = (
    <Menu>
      <Menu.Item disabled>
        <div className="flex items-center flex-row space-x-3 text-black cursor-default">
          <FullscreenOutlined
            onClick={() => setScrollable(0)}
            className="hover:text-blue-500"
          />
          <div onClick={() => setScrollable(1)} className="hover:text-blue-500">
            900px
          </div>
          <div onClick={() => setScrollable(2)} className="hover:text-blue-500">
            600px
          </div>
          <div onClick={() => setScrollable(3)} className="hover:text-blue-500">
            300px
          </div>
        </div>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item disabled className="text-black cursor-default">
        <Switch
          className="mr-2"
          size="small"
          checked={parentOn}
          onChange={(checked) => setParentOn(checked)}
        />
        Parents Always On
      </Menu.Item>
      <Menu.Item disabled className="text-black cursor-default">
        <Switch
          className="mr-2"
          size="small"
          checked={onlyToday}
          onChange={(checked) => setOnlyToday(checked)}
        />
        Only Today
      </Menu.Item>
      <Menu.Item disabled className="text-black cursor-default">
        <Switch
          className="mr-2"
          size="small"
          checked={showErrors}
          onChange={(checked) => setShowErrors(checked)}
        />
        Only Errors
      </Menu.Item>
      <Menu.Item disabled className="text-black cursor-default">
        <Switch
          className="mr-2"
          size="small"
          checked={showMainFeatures}
          onChange={(checked) => setShowMainFeatures(checked)}
        />
        Show main features
      </Menu.Item>
      <Menu.Item disabled className="text-black cursor-default">
        <Switch
          className="mr-2"
          size="small"
          checked={showSecondaryFeatures}
          onChange={(checked) => setShowSecondaryFeatures(checked)}
        />
        Show secondary features
      </Menu.Item>
      <Menu.Item>
        <Link to="/options">Settings</Link>
      </Menu.Item>
    </Menu>
  );

  const issuesWithErrors = filter(
    issues,
    (issue) =>
      issue.errors &&
      (isEmpty(selectedAvatars) ||
        (!isEmpty(selectedAvatars) &&
          issue.assignee &&
          some(
            selectedAvatars,
            ({ accountId }) => accountId === issue.assignee.accountId
          )))
  );

  return (
    <Layout className="layout">
      <Header className="bg-black text-white flex flex-row items-center space-x-2 px-1 md:px-5">
        <Input
          className="w-20"
          placeholder="Board ID"
          value={filters.boardId}
          onChange={({ target: { value: boardId } }) =>
            setFilters({ ...filters, boardId })
          }
        />
        <Select
          className="w-2/6 md:1/6 max-w-xs text-left"
          onChange={handleSprintChange}
          value={selectedSprint.id}
        >
          {sprints.map(({ id, name, state }) => {
            return (
              <Option value={id}>
                {name} - {state}
              </Option>
            );
          })}
        </Select>
        <RangePicker value={dateRange} onChange={handleDateChange} />
        <ReloadOutlined
          onClick={() => getAllIssues(selectedSprint)}
          className="text-xl"
        />
        <div className="flex-grow text-right">
          <Dropdown overlay={menu} placement="bottomRight">
            <MoreOutlined className="text-xl" />
          </Dropdown>
        </div>
      </Header>
      <Content className="bg-white" style={{ padding: "0 50px" }}>
        {isEmpty(selectedSprint) || isEmpty(issues) ? (
          <div className="flex flex-col m-20 items-center">
            <Spin
              indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
            />
            <div className="mt-5 text-gray-500">Loading...</div>
          </div>
        ) : (
          <div>
            <Team
              team={team}
              selectedAvatars={selectedAvatars}
              onAvatarChange={handleAvatarChange}
            />
            <Collapse className="mt-5" defaultActiveKey={["1"]}>
              <Panel
                header={<div className="text-left font-bold">Roadmap</div>}
                key="1"
              >
                <Roadmap
                  issues={issues}
                  sprint={selectedSprint}
                  startDate={dateRange[0]}
                  endDate={dateRange[1]}
                  scrollable={scrollable}
                  selectedAvatars={selectedAvatars}
                  parentOn={parentOn}
                  onlyWrongIssues={showErrors}
                  showMainFeatures={showMainFeatures}
                  showSecondaryFeatures={showSecondaryFeatures}
                  onlyToday={onlyToday}
                />
              </Panel>
              <Panel
                className="text-left"
                header={
                  <div className="text-left font-bold">
                    {issuesWithErrors.length} Errors
                  </div>
                }
                key="2"
              >
                <List
                  size="small"
                  bordered
                  dataSource={issuesWithErrors}
                  renderItem={({ key, summary, errors, assignee }) => (
                    <List.Item>
                      {assignee && (
                        <Avatar
                          src={assignee.avatarUrls["16x16"]}
                          size={16}
                          className="mr-1"
                        />
                      )}
                      <a
                        href={`https://pixelsandcode.atlassian.net/browse/${key}`}
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        {key}
                      </a>
                      {` ${summary} `}
                      {errors.map((tag, i) => (
                        <Tag key={i} color="red">
                          {tag}
                        </Tag>
                      ))}
                    </List.Item>
                  )}
                />
              </Panel>
            </Collapse>
          </div>
        )}
      </Content>
    </Layout>
  );
};

export default Home;
