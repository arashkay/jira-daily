/* global chrome */

const axios = require("axios");

const getAuthData = () =>
  new Promise(function (resolve) {
    chrome.storage.sync.get((value) => {
      resolve(value);
    });
  });

axios.interceptors.request.use(
  async (config) => {
    const auth = await getAuthData();
    config.auth = {
      username: auth.username,
      password: auth.token,
    };
    config.baseURL = `https://${auth.domain}.atlassian.net/`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axios;
